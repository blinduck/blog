---
title: "Why My Startup Failed & What I would have done different"  
date: 2020-11-30T21:35:31+08:00  
draft: false
---



I was the CTO of AfterYou from 2013 to about 2018. AfterYou was a home service marketplace, the idea being that you could come to our website to get housekeepers, part-time maids, contractors, plumbers or electricians. The largest portion of our business was part-time maids. You were able to chat, schedule and make payments for services online. We raised about a million dollars but we could not get the business to scale and become profitable. I started the company with some friends right out of college. Besides my inexperience, I think there are a few fundamental reasons the company failed.

1. Marketplace & Business Model

    The marketplace model is notoriously hard to get going. There is the problem of how to get customers when you don't have contractors, and vice versa. Our situation was complicated by the somewhat adverserial relationship we had with our contractors. We took a 10% cut of every service that we provided. As such, our service providers were incentivized to cut us out of the process as quickly as possible. After the initial meeting, service providers would negotiate with home owners to get paid directly. Online payment processing fees also took a substantial chunk out of our cut. 
    
    In retrospect, we should have monetized on the supply side, charging contractors a fixed recurring fee for being listed and for being a source of leads. 
   
1. Not understanding who our competition really was

    When we started the company, we though our competition was old school low tech companies that were in similar industries. You would call someone up, make an appointment over the phone and (hopefully) someone would turn up at the allotted time. There were limited reviews systems, so there was no way to know the quality of who you were engaging. 
    
    In actual fact, our biggest competition for part time home help was full time maids. Singapore  has about 200 thousand foreign domestic workers. It can cost as little as $800 to get a full time live-in maid in Singapore. Having a part time maid come in a few times a week can cost nearly as much. 
    
    While technically these workers are in Singapore with a work visa that only allows them to work for a single household, moonlighting on off days is pretty common. They can offer their services at rates that we cannot possibly hope to match.  
    
1. Tiny initial market

    Singapore is home to about 5 million people. That's a pretty small domestic market to work with. Expanding to nearby countries is hampered by serveral factors.
    
    i. The countries surrounding Singapore are very different from Singapore, speaking different languages and having very different economic circumstances. We would not have been able to simply copy and paste what we had in a different country. We did consider looking into further countries like Hong Kong and Australia, but we we did run out of money before we could execute any of our plans.
    
    ii. Our marketplace would need to be reseeded from scratch - an arduous process. 
    


    
    
    








