---
title: "Notes From Soft Skills Engineering Podcast"
date: 2021-03-17T13:25:46+08:00
draft: false
---

### How to bring up new ideas with a team when you're not the team lead?

Talk to the team lead privately. Don't spring surprises on team. Time box your new idea - say you want to try something for like 2 weeks and then do a review with the team about what they thought. Separate your ego from your idea and make sure it's easy for the team to give you feedback. 

If you have criticism, give it in private. Give praise in public. 

### What to look for in a dev team
Learning, Mission & Interesting Work/Cutting Edge.
Figure out what dysfunctions a company has and whether it's tolerable. 
Every company has dysfunction. 
Talk to different people in different roles and ask them questions to see how coherent the answers you get are.
 - How do you know what to work on each day? 
 - What processes are weak?
 - What does the company need to improve/worst part of working at the company.
 - Do you write unit tests/do your engineers write unit tests? (not writing unit tests is not a deal breaker, but you want to hear that they have a reasoned answer)

### What do you do when you feel like you're not being productive enough at work?
Take care of your motivation, get rid of distractions, make sure you're not wasting too much time.
If you're not being challenged, you should definitely bring it up with your manager.  
If you're feeling overwhelmed, ask for help early.  
Are there other systemic issues? 
1. Are you spending too many hours in meetings?
2. Are you getting too many notifications from slack/emails etc.

### Thoughts about building a personal brand
Work on things that you want to exist in the world? Most engineers don't really like spending time marketing themselves. Marketing is really important if you want to get your ideas accepted in the real world. Examining your motivations for why you want to build your personal brand is useful. 
Some companies actively try to  suppress your personal brand as it makes you less marketable. 

### Compensation
Basic salary, bonus, stock options, health benefits.
Be very comfortable asking about all the details of compensation when doing an interview. 

### Speaking at conferences
Make sure you have something worthwhile saying before pitching a con talk. It's easier if it's a personal story you have to tell about some work you've done rather than some abstract idea. You can ask the conference organizers about what kinda topics they're looking for. 
Practice your talk with a smaller audience. Meetups are an easy way to get some public speaking experience.

### Finding meaning and quitting your job
Schedule a meeting with your manager and tell them you're quitting. 




  
