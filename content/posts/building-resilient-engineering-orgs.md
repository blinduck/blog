---
title: "Some Ideas About Building Resilient Engineering Orgs"
date: 2021-02-08T21:36:00+08:00
draft: false
---

Here are some ideas about what I feel makes for a truly great engineering org.

1. Engineering driven timelines.  
    The product team (or any other team) can come up with product and feature roadmaps. They may also be responsible for prioritizing said features. However, the engineering team is responsible for coming up with timelines. Arbitrary deadlines from business/product orgs do not make sense.   

1. Regular post-mortems with a no-blame culture.  

    It's important to have regular scheduled post mortems, with the audience being the entire engineering org. Whenever a service goes down, a post mortem is conducted that covers what occurred. The 5 Whys methodology is a useful way of getting to the root cause of what went wrong. The purpose of the meeting is to uncover systematic issues that exist in the engineering organization. Are there certain patterns of failure that consistently occur? Is there a lack of metrics or alertings. 
    
    The second part of the meeting is that there needs to be action items, with clearly defined people that are responsible for them. Ideally the actions taken should make it so that it is systematically impossible for a similar failure to happen again. Humans are fallible and ideally systems should be designed so that this failure mode is not possible. Examples of systemic fixes could be:  
        i. setting up pipelines that run tests every time code is committed  
        ii. setting up linting/code tools that automatically alert you about things like potential race conditions  

1. Using code quality tools such as automatic linters are excellent. The benefits are manifold:  
    i. the entire org conforms to the same coding style. Never argue about space, tabs or brace postioning again. go-fmt(for golang) and eslint (for js) are great examples.
    ii. these tools can catch common issues such as race conditions or cyclic imports.
    
1. Automating pipelines    
    Unit tests should automatically run whenever code is pushed to a repository. Ideally deploying new code to production should be a single click of a button. Rolling back should also be a single click. 
    

     

