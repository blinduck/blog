---
title: "Notes From Designing Data Intensive Applications"
date: 2022-03-28T11:39:55+08:00
draft: false
---

## Data Models and Query Languages


When designing for SQL, we're usually looking at the real world and trying to model objects and relationships.
Database design for NoSQL (or document databases) almost works in the opposite direction. You start with the access patterns, and then you design your database in a way that allows you to get that data easily.

Because of the above, SQL can be more extensible in the long term. If you have new entities or relations, create new tables and make the relevant links with foreign keys, many-to-many relationships etc. If you need to query data in a new way, you can get efficient look up by just adding new indexes, we won't need to rearrange the data in our DB or rewrite our queries.

SOR can be more flexible, but you might end up with more application side logic that handles the cases where the schema has changed. 


### Schema on read vs Schema on write

Document databases have schema on read (SOR), where the application that reads the data assumes some kind of schema for the data thatis returned. Relational databases do schema on write(SOW), where we have to write data to the database in a particular format.

SOR can be more flexible, but you might end up with more application side logic that handles the cases where the schema has changed. 

### Storage Locality

In SQL, you've got data split across multiple tables (low storage locality) and hence queries can be slow because we need to fetch from multiple different places. If you're often accessing the entire document, NoSQL can have an advantage due to high storage locality. 

For NoSQL, your data that is queried together will often be located near each other (at least you should be designing you PK and SK so that this this true). 

### Graph-like Data Models

Good if m2m relationships are very common in your data. Store your data as vertexes as edges. You can run your regular graph algos on these data e.g. shortest points between nodes etc.
Vertexs don't have to be homegenous, can represent different entities, and any vertex can have an edge to any other vertex. Can be very flexibile if your data is highly relational.
