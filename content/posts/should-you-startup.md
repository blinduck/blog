---
title: "Should You Startup?"
date: 2020-12-28T20:27:21+08:00
draft: false
---

I failed at my first start-up. If I had known how difficult it would be, I might not have even tried the first time. So, should you start a start-up? Here are some things to consider. I'm not going to talk about the actual idea that you want to work on. To figure out if your idea is feasible and worth working on, you could read something like Peter Thiel's "Zero to One", which is excellent. This post is more about all the other things that need to be in place.  

1. Finances  
  
    When I started, I had just graduated, was not married, and lived at home with my parents. My family was solidly middle class, and finances at home were pretty comfortable. I'm generally frugal, and my monthly expenses were tiny. This made it much easier for me to start-up. If my start-up failed, I would still have no money. If my start-up succeeded, I would be rich! 
  
    Starting a start-up when you have a mortgage and a family that depends on your income might not be the best ideas. Cliche enough as it sounds, most start-ups fail. Consider that you might not bring in any money for 6-12 months. Would that be okay for you?
  
    Alternatively, you could keep you day job and work on your start-up on your own time. I think this is a very difficult thing to do if it is your first company. I worked on my company full time and I still felt like all the hours in the day were not sufficient. However, that was my first company and I had no real idea of what I was doing or about how to prioritize. The other part is psychological. Having the security of a job makes it difficult to put 100% behind your start-up. You could fail and you would still be financially secure. The stress of knowing that all your eggs are in one basket might be what you need to go that extra mile.  

2. Partners  
  
    Starting a start-up is incredibly hard. Doing it alone makes it exponentially harder. Having the right partners can either help or cripple your startup.
  
   i. Do you trust your partners?  
   ii. Have you worked together with them before? Do you work well together?  
   iii. Are all of you committed to the project at the same level? Is only one person quitting ready to quit his job and commit to this full time?  
   iv. Do your skills complement each other? If you're the technical cofounder, can your partners handle the business and sales aspects of things?
   
3. Support network

    Are the people in your life willing to support you? An early stage start-up is incredibly taxing on your personal relations. Have a chat with the important people in your life to understand how they feel. 
    
    
    `It is possible to commit no mistakes and still lose. That is not a weakness. That is life.` 
    
The above quote is especially relevant to start-ups. You could do everything right and still fail. But if you decide to start on this journey, you will find that it's almost like a turbo-booster for your skills. You will be pushed beyond what you thought you were capable of. 
    

 
    


