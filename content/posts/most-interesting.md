---
title: "Most Interesting"
date: 2021-03-23T11:27:14+08:00
draft: true
---

This is a list of media that's significantly changed the way the way I view the world.

[A Big Little Idea Called Legibility](https://www.ribbonfarm.com/2010/07/26/a-big-little-idea-called-legibility/)
Reality and real world systems are extemely complex. Without a complete understanding (which often is impossible), it is difficult to understand why systems have evolved to become the way they are. An administrator seeks to simplify these systems so as to make them

[Reality Has A Surprising Amount of Detail](http://johnsalvatier.org/blog/2017/reality-has-a-surprising-amount-of-detail)
I used to underestimate the amount of effort that goes into (seemingly) simple things. I could look at a service like instagram and think about how easy it should be to put something like that together, it's basically just a CRUD app right? This article, (and a bit more life experience) has made me really internalize that it's almost impossible to estimate the amount of effort that goes into something when taking an outside perspective. You have to actually do the work to understand the fractal nature of the space, where the details have smaller details and so on.

[Media for Thinking the Unthinkable](http://worrydream.com/MediaForThinkingTheUnthinkable/)

[The Gervais Principle](https://www.ribbonfarm.com/2009/10/07/the-gervais-principle-or-the-office-according-to-the-office/)

[The Antidote To Civilisational Collapse] (https://www.economist.com/open-future/2018/12/06/the-antidote-to-civilisational-collapse)

Quotes

```The philosopher Kwame Appiah writes that “in life, the challenge is not so much to figure out how best to play the game; the challenge is to figure out what game you’re playing.”```
