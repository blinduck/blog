---
title: "An Engineers Guide to Reviewing Designs"
date: 2020-12-08T14:58:37+08:00
draft: false
---

So you're going to be building a new product. The design team has come up with some spiffy new designs and they want to review it together with you. Here's some things to look out for..

1. This may seem obvious, but make sure there's a spec that you can compare the design against to make sure that all use cases are being covered. Designers often start with the big picture, but you're the one going to be implementing the design.

1. Design for different form factors

    Especially pertinent for the web, think about how designs will adapt to screens of different sizes.
    
    i. Wide tables - how does this look on a mobile phone?  
    ii. Are there hover interactions? What's the mobile equivalent?
    
1. Error and edge cases

    i. What happens if the user loses connectivity? Is there any graceful degradation?   
    ii. When things go wrong, do you have useful errors messages? "Oops something went wrong" is possibly the most useless error message.
   
1. How the design adapts to various amounts of data

   i. Think about how empty states for components would look.  
   ii. For large amounts of data, search and filters are essential

1. How does data update?  

   i. Is the data the user is seeing expected to be live? How often does it update? Does your data layer allow you to only fetch incremental updates?
   ii. If it's not live, are there indications about how stale the data might be?
   iii. Do you have loading states? If a user changes a filter, does the UI make it clear that a fetch is happening in the background?
 
1. Forms  

   i. Is it clear which fields are required?
   ii. Are the appropriate inputs being used? A simple input might be more user friendly then a fancy slider.






   


