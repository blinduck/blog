---
title: "Common Algos for Common Problems"
date: 2021-01-07T16:14:18+08:00
draft: true
---


### I need to know if something is present in a list  
1. Hashmap
2. Bloom filter


### Something that's supposed to happen once happened twice.

1. Distributed lock before accessing the resource
2. Concurrency token
3. Stream with once only guarantees



