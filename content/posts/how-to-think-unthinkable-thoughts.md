---
title: "How to Think Unthinkable Thoughts"
date: 2021-01-25T21:35:39+08:00
draft: true
---

Bret Victors "Media for Thinking The Unthinkable" is one of the most influential talks for me. If you haven't seen it I highy recommend checking it out.

The idea that I constantly go back to thinking about is how to think unthinkable thoughts. The idea is that we are limited by the hardware we possess, our eyes only see a portion of the electromagnetic spectrum and our ears only hear within a certain range of frequencies. Similarly, are there thoughts that we are unable to think because of how our brain is physically structured? Mathematics is one of the tools that lets us think unthinkable thoughts. There are problems that would be impossible to reason about without writing equations. T

- Compound interest?
- Maxwell's equations
- Quantum physics


Watch the talk again before coming back to this.

There is a product that I want to build around this idea. I want a desktop app like Alfred or Launchy, that's always only a key press away from jumping to the foreground. I want to treat this space like my extended mind. I'm coding, and I don't remember something, I hit the key press and I do a quick search. It brings up the notes that I'v kept about the topic in the past. I'm not looking for a generic internet search, I'm looking to create a database that's curated specially for me.
